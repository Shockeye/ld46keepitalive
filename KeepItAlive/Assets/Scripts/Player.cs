﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float walkSpeed = 1f;
    public float climbSpeed = 1f;
    public float jump = 10f;
    public float groundCheckDistance = 0.3f;
    public int health = 10;
    public float fallDamageThreshold = 20;
    Rigidbody2D rb;
    LayerMask ground;
    bool jumpFlag = false;
    bool onLadder;
    float fall = 0;
    
    Usable usable;
    int encumberance = 0;
    public int maxEncumberance = 10;
    public enum ResourceType { scrap, oxygen, water, protein, carbs};
    Dictionary<ResourceType,int> resources = new Dictionary<ResourceType,int>();
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        ground = LayerMask.GetMask("Ground");

    }


    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            jumpFlag = true;
        }
        if (Input.GetButtonDown("Use"))
        {
            Debug.Log("use1");
        }
        if (Input.GetButtonDown("Use") && usable != null)
        {
            usable.item.Use(this, usable);
        }

    }

    void FixedUpdate()
    {

        Vector2 movement = rb.velocity;

        movement.x = Input.GetAxis("Horizontal") * walkSpeed;

        if(onLadder)
        {
            movement.y = Input.GetAxis("Vertical") * climbSpeed;
        }

        if ((jumpFlag && isGrounded())|| (jumpFlag && onLadder))
        {
            movement.y = jump;
        }

        if (jumpFlag) jumpFlag = false;

        rb.velocity = movement;

        if (!(isGrounded()|| onLadder))
        {
            fall++;
        }
        else
        {
            if(fall > fallDamageThreshold) Debug.Log(fall);
            fall = 0;
        }


    }

    public bool isGrounded()
    {
        Vector3 position = transform.position;
        position.y = GetComponent<Collider2D>().bounds.min.y + 0.1f;        
        bool grounded = Physics2D.Raycast(position, Vector3.down, groundCheckDistance,ground);
        return grounded;
    }

    public void SetOnLadder(bool flag)
    {
        onLadder = flag;
    }

    public void SetUsableTarget(Usable usable)
    {
        this.usable = usable;
    }

    public void AddConsumableItem(ConsumableItem item)
    {
        if (!(encumberance > maxEncumberance))
        {
            if (resources.ContainsKey(item.resType))
            {
                resources[item.resType]++;
            }
            else
            {
                resources.Add(item.resType, 1);
            }

            encumberance++;
            
        }
       
    }
}


/*
 NOTES:
 
TODO List
resource requirements for stasis pod: nutrients, oxygen, water, power, temperature
afflictions: infection, starvation, dehydration, hypothermia, overheating
buffs: antiseptic, antibiotic, fungicide.
Robot HUD/UI

Level travel: elevators, teleport.
Robot mods/buffs

Music
Graphics

    Fall damage!

 */