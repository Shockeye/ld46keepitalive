﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ladder : MonoBehaviour
{
    void OnTriggerStay2D(Collider2D other)
    {
        Player p = other.GetComponent<Player>();
        if(p != null)
        {
            p.SetOnLadder(true);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        Player p = other.GetComponent<Player>();
        if (p != null)
        {
            p.SetOnLadder(false);
        }
    }
}
