﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Usable : MonoBehaviour
{
    public BaseItem item;

    void OnTriggerEnter2D(Collider2D other)
    {
        Player p = other.GetComponent<Player>();
        if (p != null)
        {
            p.SetUsableTarget(this);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        Player p = other.GetComponent<Player>();
        if (p != null)
        {
            p.SetUsableTarget(null);
        }
    }

}
