﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
    //Orthographic size = ((Vert Resolution)/(PPUScale * PPU)) * 0.5
    public Transform target;
    public float PPUscale = 2f;
    public float PPU = 16f;
    public float damping = .5f;

    void Start()
    {
        Camera camera = GetComponent<Camera>();
        camera.orthographicSize = (Screen.height / (PPUscale * PPU)) * 0.5f;

    }

    void FixedUpdate()
    {
        Vector3 temp = Vector3.Lerp(transform.position, target.position, Time.deltaTime * damping);
        temp.z = transform.position.z;
       
        transform.position = temp;

    }
}
