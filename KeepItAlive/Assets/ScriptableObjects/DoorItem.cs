﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DoorItem", menuName = "ScriptableObjects/DoorItem", order = 1)]
public class DoorItem : BaseItem
{
    public bool locked = false;
    public Vector3 move = new Vector3(0,256f,0);
    public override void Use(Player user, Usable usable)
    {
        if(!locked)
        {
            //open door
            usable.transform.parent.position += move; //this is a hack, obviously willll be animated later
        }
    }
}
