﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseItem : ScriptableObject
{
    public abstract void Use(Player user, Usable usable);
}
