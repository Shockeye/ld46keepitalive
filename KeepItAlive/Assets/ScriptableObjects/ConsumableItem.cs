﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ConsumableItem", menuName = "ScriptableObjects/ConsumableItem", order = 1)]
public class ConsumableItem : BaseItem
{
    public Player.ResourceType resType;
    public override void Use(Player user, Usable usable)
    {
        user.AddConsumableItem(this);
        Destroy(usable.gameObject);
        user.SetUsableTarget(null);
    }


}
