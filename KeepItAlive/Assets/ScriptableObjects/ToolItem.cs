﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ToolItem", menuName = "ScriptableObjects/ToolItem", order = 1)]
public class ToolItem : BaseItem
{
    public override void Use(Player user, Usable usable)
    {

    }
}
